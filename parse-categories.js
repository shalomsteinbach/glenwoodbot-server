'use strict';

const moment = require('moment');

const { isNow, error } = require('./utils.js');
const { RawEntry } = require('./entry.js');

module.exports = parseCategories;

function parseCategories(categories, date = moment()) {
	date = moment(date, 'YYYY-MM-DD', true);
	if (!date.isValid()) error('Received invalid date', date);

	return Object.values(
		categories
			.filter(c => isNow(c, date))
			.reduce((entriesMap, c) => {
				c.entries.forEach(e => pushPriority(new RawEntry(e, c), entriesMap));
				return entriesMap;
			}, {})
	)
		.map((e, i, entries) => e.resolve(entries, date))
		.filter(e => !!e)
		.sort(sortEntries);
}

function pushPriority(entry, entries) {
	const { priority: competing = 0 } = entries[entry.name] || {};
	if (entry.priority >= competing) entries[entry.name] = entry;
}

function sortEntries(entryA, entryB) {
	if (entryA.type === 'list') entryA = entryA.value[0];
	if (entryB.type === 'list') entryB = entryB.value[0];
	if (entryA.type === entryB.type) {
		if (entryA.type === 'time') {
			const timeA = moment(entryA.value, 'H:mm', true);
			const timeB = moment(entryB.value, 'H:mm', true);
			if (timeA.isSameOrBefore(timeB)) return -1;
			return 1;
		}
		return 0;
	}
	if (entryA.type === 'string') return -1;
	return 1;
}
