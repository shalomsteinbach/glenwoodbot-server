'use strict';

const jsep = require('jsep');
const moment = require('moment');

const { parseDate } = require('./utils.js');

class Entry {
	constructor({ category, subcategory, name, type }, value) {
		this.category = category;
		this.subcategory = subcategory;
		this.name = name;
		this.type = type;
		this.value = value;
	}
}

class RawEntry {
	constructor(entry, { category = null, subcategory = null, priority = 0 }) {
		if (!isEntryValid(entry)) error('Received malformed entry', entry);
		this.category = category;
		this.subcategory = subcategory;
		this.priority = priority;
		this.name = entry.name;
		this.type = entry.type;
		this.value = entry.value;
	}

	resolve(entries, date) {
		switch (this.type) {
			case 'string':
			case 'time':
				this.resolved = new Entry(this, this.value);
				break;

			case 'list':
				this.resolved = resolveListValue(this, entries, date);
				break;

			case 'occurs':
				this.resolved = resolveOccursValue(this, entries, date);
				break;

			case 'expiration':
				this.resolved = resolveExpiresValue(this, entries, date);
				break;

			case 'expression':
				this.resolved = resolveExpressionValue(this, entries, date);
				break;

			default:
				error('Attempt to resolve entry with unknown type', entry.type);
		}
		return this.resolved;
	}
}

function resolveListValue(entry, entries, date) {
	return new Entry(
		entry,
		entry.value.map(val => new RawEntry(val, entry).resolve(entries, date))
	);
}

function resolveOccursValue(entry, entries, date) {
	for (const value of entry.value) {
		const occurs = parseDate(value.occurs, date);
		if (date.isSame(occurs, 'day'))
			return new RawEntry({ ...value.value, name: entry.name }, entry).resolve(
				entries,
				date
			);
	}
}

function resolveExpiresValue(entry, entries, date) {
	for (const value of entry.value) {
		const expiration = parseDate(value.expiration, date);
		if (date.isSameOrBefore(expiration, 'day'))
			return new RawEntry({ ...value.value, name: entry.name }, entry).resolve(
				entries,
				date
			);
	}
}

function resolveExpressionValue(entry, entries, date) {
	const expressionTree = jsep(entry.value);
	let iter = calculateExpression(expressionTree);
	let { value, done } = iter.next();
	while (!done) {
		const referencedEntry = entries.find(e => e.name === value);
		if (referencedEntry === undefined) {
			console.warn("Bad expression. Couldn't find referenced entry: " + value);
			return;
		}

		const resolvedValue = referencedEntry.resolve(entries, date).value;
		if (resolvedValue === undefined) return;

		const next = iter.next(resolvedValue);
		value = next.value;
		done = next.done;
	}
	return new Entry({ ...entry, type: 'time' }, value);
}

function* calculateExpression(node) {
	if (node.type === 'Literal') return node.value;

	if (node.type === 'Identifier') return yield node.name;

	if (node.type === 'BinaryExpression') {
		const { left, right, operator } = node;
		const lval = yield* calculateExpression(left);
		const rval = yield* calculateExpression(right);
		return timeOps[operator](lval, rval);
	}

	if (node.type === 'MemberExpression') {
		const { object, property } = node;
		const value = yield* calculateExpression(object);
		const index = yield* calculateExpression(property);
		return value[index];
	}
}

const timeOps = {
	'+': (lval, rval) => {
		const { time, offset } = orderTimeAndOffset(lval, rval);
		return moment(time, 'H:mm', true)
			.add(offset, 'm')
			.format('H:mm');
	},
	'-': (lval, rval) => {
		return moment(lval, 'H:mm', true)
			.subtract(rval, 'm')
			.format('H:mm');
	}
	// TODO: make more operations
};

function orderTimeAndOffset(lval, rval) {
	if (moment(lval, 'H:mm').isValid()) return { time: lval, offset: rval };
	return { time: rval, offset: lval };
}

function isEntryValid(entry) {
	if (!entry) return false;
	const typeofType = typeof entry.type;
	const typeofValue = typeof entry.value;
	return (
		typeofType === 'string' &&
		(typeofValue === 'object' || typeofValue === 'string')
	);
}

module.exports = {
	Entry,
	RawEntry
};
