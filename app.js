'use strict';

const express = require('express'),
	server = express();
const { dialogflow } = require('actions-on-google');
const { getString } = require('./utils.js');
const {
	getTodaysSchedule,
	getCategorySchedule,
	getEntry
} = require('./queries.js');

const app = dialogflow();

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => console.log('App started running on port: ' + PORT));

server.use('/', express.static('public'));
server.all('/', express.json(), app);

app.intent('get.todays_schedule', conv => {
	const [supplyResponse, ...followupResponse] = conv.incoming;
	conv.ask(supplyResponse, getTodaysSchedule(), ...followupResponse);
});

app.intent('get.category_schedule', (conv, { category }) => {
	const [supplyResponse, ...followupResponse] = conv.incoming;
	const categorySchedule = getCategorySchedule(category);
	if (!categorySchedule) conv.followup('CATEGORY_NOT_FOUND');
	conv.ask(supplyResponse, categorySchedule, ...followupResponse);
});

app.intent('get.entry', (conv, { general_entry }) => {
	const [supplyResponse, ...followupResponse] = conv.incoming;
	const entryValue = getEntry(general_entry);
	if (!entryValue) conv.followup('ENTRY_NOT_FOUND');
	else {
		supplyResponse.textToSpeech += ' ' + entryValue;
		supplyResponse.textToSpeech.replace(
			general_entry,
			getString(general_entry)
		);
		conv.ask(supplyResponse, ...followupResponse);
	}
});
