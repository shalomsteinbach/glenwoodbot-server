'use strict';

const moment = require('moment');
const { Table } = require('actions-on-google');

const parseCategories = require('./parse-categories.js');
const categories = require('./res/categories.json');
const { flattenEntry, getNextOccurrence, getString } = require('./utils.js');

module.exports = {
	getTodaysSchedule() {
		return tablify(
			'Today',
			moment().format('DD/MM/YY'),
			parseCategories(categories)
		);
	},

	getCategorySchedule(category) {
		if (category === 'shabbat') return _getShabbatSchedule();
		const jsonCategory = categories.find(c => c.name === category);
		if (!jsonCategory) return;
		const nextOccurrence = getNextOccurrence(category);
		if (nextOccurrence)
			return tablify(
				category,
				nextOccurrence.format('DD/MM/YY'),
				parseCategories([jsonCategory])
			);
	},

	getEntry(general_entry) {
		const entry = parseCategories(categories).find(
			e => e.name === general_entry
		);
		return entry && flattenEntry(entry);
	}
};

function tablify(title, subtitle, ...entriesSections) {
	const rows = entriesSections.reduce((reducedRows, entries) => {
		const sectionRows = entries.map(e => ({
			cells: [flattenEntry(e), getString(e.name)]
		}));
		sectionRows[sectionRows.length - 1].dividerAfter = true;
		return reducedRows.concat(sectionRows);
	}, []);

	return new Table({
		title,
		subtitle,
		columns: [
			{
				align: 'LEADING'
			},
			{
				align: 'TRAILING'
			}
		],
		rows
	});
}

function _getShabbatSchedule() {
	const friday = moment('fri', 'ddd', true);
	const saturday = moment('sat', 'ddd', true);
	const generalEntries = parseCategories(categories, saturday).filter(
		e => e.category === 'shabbat' && e.subcategory === 'general'
	);
	const eveEntries = parseCategories(categories, friday).filter(
		e => e.category === 'shabbat' && e.subcategory === 'eve'
	);
	const dayEntries = parseCategories(categories, saturday).filter(
		e => e.category === 'shabbat' && e.subcategory === 'day'
	);
	return tablify(
		'Shabbat (Fri - Sat)',
		`${friday.format('DD/MM/YY')} - ${saturday.format('DD/MM/YY')}`,
		generalEntries,
		eveEntries,
		dayEntries
	);
}
