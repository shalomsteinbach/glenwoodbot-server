'use strict';

const moment = require('moment');

module.exports = {
	error,
	flattenEntry,
	getNextOccurrence,
	getString,
	isNow,
	parseDate
};

function error(message, badValue) {
	throw Error(`${__filename}: ${message}\n${JSON.stringify(badValue)}`);
}

function parseDate(str, now) {
	const parsed = moment(str, 'YYYY-MM-DD', true);
	if (parsed.isValid()) return parsed;
	if (moment(str, 'ddd', true).isValid) return moment(now).day(str);
	error('Received date string of an unknown format', str);
}

function getNextOccurrence({ occurs }, now = moment()) {
	if (!occurs) return now;
	if (Array.isArray(occurs)) {
		const nextOccurrenceStr = occurs.find(o =>
			now.isSameOrBefore(parseDate(o, now), 'd')
		);
		return nextOccurrenceStr && parseDate(nextOccurrenceStr, now);
	}
	const occurrence = parseDate(occurs, now);
	if (now.isSameOrBefore(occurrence, 'd')) return occurrence;
}

function isNow(category, now = moment()) {
	return now.isSame(getNextOccurrence(category, now), 'd');
}

function getString(string) {
	return require('./res/strings.json')[string] || string;
}

function flattenEntry(entry) {
	if (entry.type === 'list') return entry.value.map(v => v.value).join(', ');
	return entry.value;
}
